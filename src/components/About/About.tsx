import React from "react";
import styles from "./About.module.scss";

const About: React.FC = () => {
  return (
    <div className={styles.wrapper}>
      <div>About</div>
    </div>
  );
};

export default About;
