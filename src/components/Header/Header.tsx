import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./Header.module.scss";

const Header: React.FC = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.name}>
        <NavLink to="/">MAGOMED MURDALOV</NavLink>
      </div>
      <div className={styles.location}>BASED IN GROZNY</div>
      <div className={styles.itemsCont}>
        <div className={styles.item}>
          <NavLink to="/work">WORK,</NavLink>
        </div>
        <div className={styles.item}>
          <NavLink to="/about">ABOUT,</NavLink>
        </div>
        <div className={styles.item}>
          <NavLink to="/contact">CONTACT</NavLink>
        </div>
      </div>
    </div>
  );
};

export default Header;
