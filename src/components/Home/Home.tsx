import React from "react";
import styles from "./Home.module.scss";
import image from "./../../assets/images/image1.png";
import { NavLink } from "react-router-dom";

const Home: React.FC = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.firstCont}>
          <div className={styles.title}>
            <span>FRONTEND</span>
            <span>DEVELOPER</span>
          </div>
        </div>
        <div className={styles.secondCont}>
          <div className={styles.image}>
            <img src={image} alt="" />
          </div>
          <div className={styles.titleCont}>
            <div className={styles.item}>
              <NavLink to="/work">WORK</NavLink>
            </div>
            <div className={styles.title}>
              <span>MAGOMED</span>
              <span>MURDALOV</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
