import React from "react";
import "./App.css";
import { Route, Routes, useLocation } from "react-router-dom";
import Home from "./components/Home/Home";
import Header from "./components/Header/Header";
import Work from "./components/Work/Work";
import About from "./components/About/About";
import Contact from "./components/Contact/Contact";
import { TransitionGroup, CSSTransition } from "react-transition-group";

const App: React.FC = () => {
  const location = useLocation();

  return (
    <div className="app-wrapper">
      <div className="app-wrapper-content">
        <Header />
        <TransitionGroup>
          <CSSTransition timeout={250} classNames="fade" key={location.key}>
            <Routes location={location}>
              <Route path="/" element={<Home />} />
              <Route path="/work" element={<Work />} />
              <Route path="/about" element={<About />} />
              <Route path="/contact" element={<Contact />} />
            </Routes>
          </CSSTransition>
        </TransitionGroup>
      </div>
    </div>
  );
};

export default App;
